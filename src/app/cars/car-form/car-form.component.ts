import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Car} from "../car";

@Component({
  selector: 'app-car-form',
  templateUrl: './car-form.component.html',
  styleUrls: ['./car-form.component.css']
})
export class CarFormComponent implements OnInit {

  @Input() car?: Car;
  @Output() carEmitter = new EventEmitter<Car>();
  from!: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.from = this.fb.group({
      brand: this.car?.brand || '',
      model: this.car?.model || '',
      year: this.car?.year || '',
    });
  }

  onSave() {
    this.carEmitter.emit(this.from.value);
  }
}
