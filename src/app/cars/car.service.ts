import { Injectable } from '@angular/core';
import {Car} from "./car";
import {BehaviorSubject, Observable} from "rxjs";
import * as _ from "lodash";

@Injectable({
  providedIn: 'root'
})
export class CarService {

  private cars$ = new BehaviorSubject<Car[]>([]);

  addCar(car: Car) {
    const cars = this.cars$.getValue();
    cars.push(car);
    this.cars$.next(cars);
  }

  editCar(car: Car, index: number) {
    const cars = this.cars$.getValue();
    cars[index] = car;
    this.cars$.next(cars);
  }

  removeCar(index: number) {
    const cars = this.cars$.getValue();
    cars.splice(index, 1);
    this.cars$.next(cars);
  }

  getCars(): Observable<Car[]> {
    return this.cars$.asObservable();
  }

  sort(field: string, mode: "asc" | "desc") {
    const cars = this.cars$.getValue();
    const sorted = _.orderBy(cars, field, mode);
    this.cars$.next(sorted);
  }
}
