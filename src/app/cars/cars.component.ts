import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {CarFormComponent} from "./car-form/car-form.component";
import {Car} from "./car";
import {CarService} from "./car.service";
import {Subscription} from "rxjs";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent {

  filter = new FormControl('');
  cars$ = this.carService.getCars();

  constructor(private modalService: NgbModal, private carService: CarService) { }

  openAddCarForm() {
    const modalRef = this.modalService.open(CarFormComponent);
    modalRef.componentInstance.carEmitter.subscribe((carSaved: Car) => {
      this.carService.addCar(carSaved);
      modalRef.close();
    });
  }

  openEditCarForm(car: Car, index: number) {
    const modalRef = this.modalService.open(CarFormComponent);
    modalRef.componentInstance.car = car;
    modalRef.componentInstance.carEmitter.subscribe((carSaved: Car) => {
      this.carService.editCar(carSaved, index);
      modalRef.close();
    });
  }

  removeCar(index: number) {
    if (confirm("Czy napewno usunąć?")) {
      this.carService.removeCar(index);
    }
  }

  sort(field: string, mode: 'asc' | 'desc') {
    this.carService.sort(field, mode);
  }
}
