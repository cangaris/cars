<h1>Zadanie rekrutacyjne</h1>

W ramach pierwszego etapu rekrutacji prosimy o przygotowanie elementu, korzystając z HTML + CSS + JS.<br>
Mile widziane wykorzystanie Angular (https://github.com/angular/angular-cli) i/lub jQuery, Bootstrap.<br>

Należy przygotować widok, który będzie zawierać tabelę z trzema kolumnami:<br>
- Marka pojazdu,<br>
- Model pojazdu,<br>
- Rok produkcji.<br>

Tabela ma pozwalać na sortowanie po każdej z kolumn oraz filtrowanie po modelu pojazdu.<br>
Użytkownik ma mieć możliwość dodania nowego rekordu do tabeli oraz edycji widocznych na liście samochodów w oknie modalnym. Przygotowana ma zostać również funkcjonalność usunięcia rekordu z tabeli.<br>

Wymagania<br>
- Wykorzystanie HTML+CSS+JS z Bootstrap 5,<br>
- Utworzenie modułu, komponentu, serwisu.<br>

Mile widziane<br>
- Wykorzystanie zewnętrznej biblioteki npm,<br>
- Wykorzystanie Angular i/lub jQuery, Bootstrap.<br>
